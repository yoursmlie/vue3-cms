import { ref } from "vue";
import pageContent from "@/components/pageContent/index";
export function usePageSearch() {
  const pageContentRef = ref<InstanceType<typeof pageContent>>();
  const handleQueryClick = (queryInfo: any) => {
    pageContentRef.value?.getPageData(queryInfo);
  };
  const handleResetClick = () => {
    pageContentRef.value?.getPageData();
  };
  return [pageContentRef, handleQueryClick, handleResetClick];
}
