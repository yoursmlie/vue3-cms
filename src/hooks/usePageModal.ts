import { ref } from "vue";
import pageModal from "@/components/pageModal/index";
type CallbackFn = (item?: any) => void;
export function usePageModal(
  newCallback?: CallbackFn,
  editCallback?: CallbackFn
) {
  const pageModalRef = ref<InstanceType<typeof pageModal>>();
  const defaultInfo = ref({});
  const handleNewData = () => {
    defaultInfo.value = {};
    if (pageModalRef.value) pageModalRef.value.dialogVisible = true;
    newCallback && newCallback();
  };
  const handleEditData = (item: any) => {
    defaultInfo.value = { ...item };
    if (pageModalRef.value) {
      pageModalRef.value.dialogVisible = true;
    }
    editCallback && editCallback(item);
  };
  return [pageModalRef, defaultInfo, handleNewData, handleEditData];
}
