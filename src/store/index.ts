import { createStore, Store, useStore as useVuexStore } from "vuex";
import { IRootState, StoreType } from "./types";
import { getPageListData } from "@/service/main/system/system";
import login from "./login/login";
import system from "./main/system/system";
import dashboard from './main/analysis/dashboard'
const store = createStore<IRootState>({
  state: () => {
    return {
      name: "xiaoming",
      age: 12,
      entireDepartment: [],
      entireRole: [],
      entirMenu: [],
    };
  },
  mutations: {
    changeEntireDeparment(state, list) {
      state.entireDepartment = list;
    },
    changeEntireRole(state, list) {
      state.entireRole = list;
    },
    changeEntirMenu(state, list) {
      state.entirMenu = list;
    },
  },
  getters: {},
  actions: {
    async getInitialDataAction({ commit }) {
      // 1. 请求部门和角色数据
      const depResult = await getPageListData("/department/list", {
        offset: 0,
        size: 1000,
      });
      const { list: depLsit } = depResult.data;
      const roleResult = await getPageListData("/role/list", {
        offset: 0,
        size: 1000,
      });
      const { list: roleList } = roleResult.data;
      commit("changeEntireDeparment", depLsit);
      commit("changeEntireRole", roleList);
      const menuResult = await getPageListData("/menu/list", {});
      const { list: menuList } = menuResult.data;
      commit("changeEntirMenu", menuList);
    },
  },
  modules: {
    login,
    system,
    dashboard
  },
});

export function setupStore() {
  store.dispatch("login/loadLocalLogin");
}

export function useStore(): Store<StoreType> {
  return useVuexStore();
}

export default store;
