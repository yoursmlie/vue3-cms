import { ILoginState } from "./login/types";
import { SysetmState } from "./main/system/types";
import { DashBoardState } from "./main/analysis/types";
export interface IRootState {
  name: string;
  age: number;
  entireDepartment: any[];
  entireRole: any[];
  entirMenu: any[];
}

export interface RootWithModule {
  login: ILoginState;
  system: SysetmState;
  dashboard: DashBoardState
}

export type StoreType = IRootState & RootWithModule;
