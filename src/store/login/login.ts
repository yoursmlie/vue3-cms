import { Module } from "vuex";
import { ILoginState } from "./types";
import { IRootState } from "../types";
import { router } from "@/router/index";
import LocalCache from "@/utils/cache";
import {
  accountLoginRequest,
  requestUserInfoById,
  requestUserMenusByRoleId,
} from "@/service/login/login";
import { Account } from "@/service/login/types";
import { mapMenusToRoutes, mapMenusToPermissions } from "@/utils/mapMenus";
//Module<S,R> S模块中state的类型，R根store中的state类型
const loginModule: Module<ILoginState, IRootState> = {
  namespaced: true,
  state() {
    return {
      token: "",
      userInfo: {},
      userMenus: [],
      permissions: [],
    };
  },
  mutations: {
    changeToken(state, token: string) {
      state.token = token;
    },
    changeUserInfo(state, userInfo: any) {
      state.userInfo = userInfo;
    },
    changeUserMenus(state, userMenus: any) {
      state.userMenus = userMenus;
      //userMenus => routes
      //将routes => router.main.children
      const routes = mapMenusToRoutes(userMenus);
      routes.forEach((route) => {
        router.addRoute("main", route);
      });
      const permissions = mapMenusToPermissions(userMenus);
      state.permissions = permissions;
      console.log(permissions);
    },
  },
  actions: {
    async accountLoginAction({ commit, dispatch }, payload: Account) {
      //登录逻辑
      const loginResult = await accountLoginRequest(payload);
      const { id, token } = loginResult.data;
      commit("changeToken", token);
      LocalCache.setCache("token", token);
      dispatch("getInitialDataAction", null, { root: true });
      //请求用户信息数据
      const userInfoResult = await requestUserInfoById(id);
      const userInfo = userInfoResult.data;
      commit("changeUserInfo", userInfo);
      LocalCache.setCache("userInfo", userInfo);
      //请求用户菜单
      const userMenusResult = await requestUserMenusByRoleId(userInfo.role.id);
      const userMenus = userMenusResult.data;
      commit("changeUserMenus", userMenus);
      LocalCache.setCache("userMenus", userMenus);
      //跳到首页
      router.push("/main");
    },
    loadLocalLogin({ commit, dispatch }) {
      const token = LocalCache.getCache("token");
      if (token) {
        commit("changeToken", token);
        dispatch("getInitialDataAction", null, { root: true });
      }
      const userInfo = LocalCache.getCache("userInfo");
      if (userInfo) {
        commit("changeUserInfo", userInfo);
      }
      const userMenus = LocalCache.getCache("userMenus");
      if (userMenus) {
        commit("changeUserMenus", userMenus);
      }
    },
    phoneLoginAction({ commit }, payload) {
      console.log("loginPhone", payload);
    },
  },
};

export default loginModule;
