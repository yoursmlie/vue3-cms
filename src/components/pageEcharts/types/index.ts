export interface DataType {
  name: string;
  value: any;
}
