import { App } from 'vue'
import registerElement from './registerElement'
import registerProperties from './registerProperites'

export function registerApp(app: App): void {
  registerElement(app)
  app.use(registerProperties)
}
