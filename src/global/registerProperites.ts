import { App } from 'vue'
import { formatUtcString } from '@/utils/dateFormat'
export default function registerProperties(app: App) {
  //全局注册属性
  app.config.globalProperties.$filters = {
    formatTime(value: string) {
      return formatUtcString(value);
    },
  };
}
