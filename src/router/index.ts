import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import { firstMenu } from '@/utils/mapMenus'
import LocalCache from "@/utils/cache";
const routes: RouteRecordRaw[] = [
  {
    path: "/",
    redirect: "/main",
  },
  {
    path: "/login",
    name: "login",
    component: () => import("@/views/login/login.vue"),
  },
  {
    path: "/main",
    name: "main",
    component: () => import("@/views/main/main.vue"),
    // children: [] -> 根据userMenus来决定
  },
  {
    path: "/:pathMatch(.*)*",
    name: "notFound",
    component: () => import("@/views/notFound/notFound.vue"),
  },
];

export const router = createRouter({
  routes,
  history: createWebHashHistory(),
});

router.beforeEach((to) => {
  if (to.path !== "/login") {
    const token = LocalCache.getCache("token");
    if (!token) {
      return "/login";
    }
    if(to.path === '/main') {
      return firstMenu.url
    }
  }
});
