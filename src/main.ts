import { createApp } from "vue";
import { router } from "./router";
import { registerApp } from "./global/index";
import "normalize.css";
import "./assets/css/index.less";
import store from "./store";
import { setupStore } from "./store/index";
import routeApp from "./App.vue";

const app = createApp(routeApp);
registerApp(app);
setupStore();
app.use(router);
app.use(store);
app.mount("#app");
