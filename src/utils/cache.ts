class LocalCache {
  setCache(key: string, value: any, isLocalStorage: boolean = true) {
    if (isLocalStorage) {
      window.localStorage.setItem(key, JSON.stringify(value))
    }
  }
  getCache(key: string, isLocalStorage: boolean = true) {
    if(isLocalStorage) {
      const value = window.localStorage.getItem(key)
      if(value) {
        return JSON.parse(value)
      }
    }
  }
  deleteCache(key: string, isLocalStorage: boolean = true) {
    if(isLocalStorage) {
      window.localStorage.removeItem(key)
    }
  }
  clearCache(isLocalStorage: boolean = true) {
    if(isLocalStorage) {
      window.localStorage.clear()
    }
  }
}

export default new LocalCache();
