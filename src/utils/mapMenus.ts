import { RouteRecordRaw } from "vue-router";
import { Breadcrumb } from "@/base-ui/breadcrumb/index";

let firstMenu: any = null;

export function mapMenusToRoutes(userMenus: any[]): RouteRecordRaw[] {
  const routes: RouteRecordRaw[] = [];
  //1. 先去加载默认所有的routes
  const allRoutes: RouteRecordRaw[] = [];
  const routeFiles = require.context("../router/main", true, /\.ts/); //webpack工具
  routeFiles.keys().forEach((key) => {
    //console.log(key); //文件路径
    const route = require("../router/main" + key.split(".")[1]);
    allRoutes.push(route.default);
  });
  //2. 根据菜单获取需要添加的routes
  const recurseGetRoute = (menus: any[]) => {
    for (const menu of menus) {
      if (menu.type === 2) {
        const route = allRoutes.find((route) => {
          return route.path === menu.url;
        });
        if (route) routes.push(route);
        if (!firstMenu) {
          firstMenu = menu;
        }
      } else {
        recurseGetRoute(menu.children);
      }
    }
  };

  recurseGetRoute(userMenus);
  return routes;
}

export function pathMenusBreadcrumbs(userMenus: any[], currentPath) {
  const breadcrumbs: Breadcrumb[] = [];
  pathMapToMenu(userMenus, currentPath, breadcrumbs);
  return breadcrumbs;
}

export function pathMapToMenu(
  userMenus: any[],
  currentPath: string,
  breadcrumbs?: Breadcrumb[]
) {
  for (const menu of userMenus) {
    if (menu.type === 1) {
      const findMenu = pathMapToMenu(menu.children ?? [], currentPath);
      if (findMenu) {
        breadcrumbs?.push({ name: menu.name });
        breadcrumbs?.push({ name: findMenu.name });
        return findMenu;
      }
    } else if (menu.type === 2 && menu.url === currentPath) {
      return menu;
    }
  }
}

export function mapMenusToPermissions(userMenus: any[]) {
  const permissions: string[] = [];
  const recurseGetPermission = (menus: any[]) => {
    for (const menu of menus) {
      if (menu.type === 1 || menu.type === 2) {
        recurseGetPermission(menu.children ?? []);
      } else if (menu.type === 3) {
        permissions.push(menu.permission);
      }
    }
  };
  recurseGetPermission(userMenus);
  return permissions;
}

export function getMneuLeafKeys(menuList: any[]) {
  const leafKeys: number[] = [];
  const recurseGetLeaf = (menuList: any[]) => {
    for (const menu of menuList) {
      if (menu.children) {
        recurseGetLeaf(menu.children);
      } else {
        leafKeys.push(menu.id);
      }
    }
  };
  recurseGetLeaf(menuList);
  return leafKeys
}

export { firstMenu };
