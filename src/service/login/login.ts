import Request from "../index";
import { Data } from '@/service/types'
import { Account, LoginResult } from "./types";
enum LoginAPI {
  AccountLogin = "/login",
  LoginUserInfo = "/users/",
  UserMenus = '/role/'
}
export function accountLoginRequest(account: Account) {
  return Request.post<Data<LoginResult>>({
    url: LoginAPI.AccountLogin,
    data: account,
  });
}

export function requestUserInfoById(id: number) {
  return Request.get<Data>({
    url: LoginAPI.LoginUserInfo + id,
    showLoading: false
  })
}

export function requestUserMenusByRoleId(id: number) {
  return Request.get<Data>({
    url: LoginAPI.UserMenus + id + '/menu',
    showLoading: false
  })
}
