export interface Account {
  name: string;
  password: string;
}

export interface LoginResult {
  id: number;
  name: string;
  token: string;
}
