import axios from "axios";
import type { AxiosInstance } from "axios";
import type { RequestConifg, RequestInterceptors } from "./types";
import { ElLoading } from "element-plus";
import { ILoadingInstance } from "element-plus/lib/el-loading/src/loading.type";

const DEFAULT_LOADING = true;

class Request {
  instance: AxiosInstance;
  interceptors?: RequestInterceptors;
  loading?: ILoadingInstance;
  showLoading: boolean;
  constructor(config: RequestConifg) {
    //创建axios实例
    this.instance = axios.create(config);
    //保存基本信息
    this.showLoading = config.showLoading ?? DEFAULT_LOADING;
    this.interceptors = config.interceptors;
    //使用拦截器
    //从config中取出的拦截器是对于的实例的拦截器
    this.instance.interceptors.request.use(
      this.interceptors?.requestInterceptor,
      this.interceptors?.requestInterceptorCatch
    );
    this.instance.interceptors.response.use(
      this.interceptors?.reponseInterceptor,
      this.interceptors?.reponseInterceptorCatch
    );
    //添加所有的实例都有的拦截器
    this.instance.interceptors.request.use(
      (config) => {
        if (this.showLoading) {
          this.loading = ElLoading.service({
            lock: true,
            text: "正在请求数据...",
          });
        }
        return config;
      },
      (err) => {
        return err;
      }
    );
    this.instance.interceptors.response.use(
      (res) => {
        this.loading?.close();
        return res.data;
      },
      (err) => {
        if (err.response.status === 404) {
          console.log("404错误");
        }
        return err;
      }
    );
  }
  request<T = any>(config: RequestConifg<T>): Promise<T> {
    return new Promise((resolve, reject) => {
      if (config.interceptors?.requestInterceptor) {
        config = config.interceptors.requestInterceptor(config);
      }
      if (config.showLoading === false) {
        this.showLoading = config.showLoading;
      }
      console.log(config)
      this.instance
        .request<any, T>(config)
        .then((res) => {
          if (config.interceptors?.reponseInterceptor) {
            res = config.interceptors.reponseInterceptor(res);
          }
          this.showLoading = DEFAULT_LOADING;
          resolve(res)
        })
        .catch((err) => {
          this.showLoading = DEFAULT_LOADING;
          reject(err)
          console.log(err);
        });
    });
  }
  get<T = any>(config: RequestConifg<T>): Promise<T> {
    return this.request<T>({...config, method: 'GET'})
  }
  post<T = any>(config: RequestConifg<T>): Promise<T> {
    console.log(config)
    return this.request<T>({...config, method: 'POST'})
  }
  delete<T = any>(config: RequestConifg<T>): Promise<T> {
    return this.request<T>({...config, method: 'DELETE'})
  }
  patch<T = any>(config: RequestConifg<T>): Promise<T> {
    return this.request<T>({...config, method: 'PATCH'})
  }
}

export default Request;
