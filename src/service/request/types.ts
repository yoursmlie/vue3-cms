import type { AxiosRequestConfig, AxiosResponse } from "axios";

export interface RequestInterceptors<T = AxiosResponse> {
  requestInterceptor?: (config: AxiosRequestConfig) => AxiosRequestConfig;
  requestInterceptorCatch?: (error: any) => any;
  reponseInterceptor?: (res: T) => T;
  reponseInterceptorCatch?: (error: any) => any;
}

export interface RequestConifg<T = AxiosResponse> extends AxiosRequestConfig {
  interceptors?: RequestInterceptors<T>;
  showLoading?: boolean;
}
