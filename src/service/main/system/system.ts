import Request from "../../index";
import { Data } from "@/service/types";
export function getPageListData(url: string, queryInfo: any) {
  return Request.post<Data>({
    url,
    data: queryInfo,
  });
}
export function deletePageData(url: string) {
  return Request.delete<Data>({
    url,
  });
}
export function createPageData(url: string, newData: any) {
  return Request.post<Data>({
    url,
    data: newData,
  });
}

export function editPageData(url: string, editData: any) {
  return Request.patch<Data>({
    url,
    data: editData,
  });
}

