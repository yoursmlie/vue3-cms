import Request from "../../index";
enum DashboardApi {
  categoryGoodsCount = "/goods/category/count",
  categoryGoodsSale = "/goods/category/sale",
  categoryGoodsFavor = "/goods/category/favor",
  addressGoodsSale = "/goods/address/sale",
}

export function getCategoryGoodsCount() {
  return Request.get({
    url: DashboardApi.categoryGoodsCount,
  });
}

export function getCategoryGoodsSale() {
  return Request.get({
    url: DashboardApi.categoryGoodsSale,
  });
}

export function getCategoryGoodsFavor() {
  return Request.get({
    url: DashboardApi.categoryGoodsFavor,
  });
}

export function getAddressGoodsSale() {
  return Request.get({
    url: DashboardApi.addressGoodsSale,
  });
}
