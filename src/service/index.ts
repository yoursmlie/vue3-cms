import RequestConfig from "./request/index";
import { BASE_URL, TIME_OUT } from "./request/config";
import LocalCache from '@/utils/cache'
export default new RequestConfig({
  baseURL: BASE_URL,
  timeout: TIME_OUT,
  interceptors: {
    requestInterceptor: (config) => {
      const token = LocalCache.getCache('token');
      if (token) {
        config.headers.Authorization = `Bearer ${token}`;
      }
      return config;
    },
    requestInterceptorCatch: (err) => {
      return err;
    },
    reponseInterceptor: (res) => {
      console.log("响应的拦截");
      return res;
    },
    reponseInterceptorCatch: (err) => {
      return err;
    },
  },
});
