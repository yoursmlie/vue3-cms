import { DefinForm } from "@/base-ui/index";
export const formConfig: DefinForm = {
  labelWidth: "120px",
  itemLayout: {
    padding: "10px 40px",
  },
  colLayout: {
    span: 7,
  },
  formItems: [
    {
      fieId: "name",
      type: "input",
      label: "角色名",
      rules: [],
      placeholder: "请输入角色名",
    },
    {
      fieId: "intro",
      type: "input",
      label: "权限介绍",
      rules: [],
      placeholder: "请输入权限介绍",
    },
    {
      fieId: "createTime",
      type: "datepicker",
      label: "创建时间",
      otherOptions: {
        startPlaceholder: "开始时间",
        endPlaceholder: "结束时间",
        type: "daterange",
      },
    },
  ],
};
