const title = "商品列表";
const showIndexColumn = true;
const showSelectColumn = true;
const propList = [
  {
    prop: "name",
    label: "商品名称",
    minWidth: 100,
    slotName: "name",
  },
  {
    prop: "oldPrice",
    label: "原价格",
    minWidth: 100,
    slotName: "oldPrice",
  },
  {
    prop: "newPrice",
    label: "现价格",
    minWidth: 100,
    slotName: "newPrice",
  },
  {
    prop: "status",
    label: "状态",
    minWidth: 100,
    slotName: "status",
  },
  {
    prop: "desc",
    label: "商品介绍",
    minWidth: 160,
    slotName: "desc",
  },
  {
    prop: "createAt",
    label: "创建时间",
    minWidth: 250,
    slotName: "createAt",
  },
  {
    prop: "imgUrl",
    label: "商品图片",
    minWidth: 100,
    slotName: "image",
  },
  {
    prop: "address",
    label: "生产地",
    minWidth: 100,
    slotName: "address",
  },
  {
    prop: "createAt",
    label: "创建时间",
    minWidth: 160,
    slotName: "createAt",
  },
  {
    prop: "updateAt",
    label: "更新时间",
    minWidth: 160,
    slotName: "updateAt",
  },
];

export const contentTableConifg = {
  title,
  showIndexColumn,
  showSelectColumn,
  propList,
};
