type FormType = "input" | "password" | "select" | "datepicker";
interface optionsType {
  title: string;
  value: string | number;
}
export interface FormItem {
  fieId: string;
  type: FormType;
  label: string;
  rules?: any[];
  placeholder?: any;
  //select
  options?: optionsType[];
  //特殊的属性
  otherOptions?: {} | string | Boolean;
  isHidden?: boolean;
}

export interface DefinForm {
  formItems?: FormItem[];
  labelWidth?: string;
  colLayout?: any;
  itemLayout?: any;
}
