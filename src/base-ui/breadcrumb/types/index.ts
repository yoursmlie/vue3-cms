export interface Breadcrumb {
  name: string,
  path?: ''
}
