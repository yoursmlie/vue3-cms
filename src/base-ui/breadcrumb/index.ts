import navBreadCrumb from "./src/navBreadCrumb.vue";

export * from "./types";

export default navBreadCrumb;
